from rest_framework import viewsets, filters, generics
from serializers import SongsSerializer
from pages.models import Song
from paginators import AutoloadPagination

class SongViewSet(generics.ListAPIView):
    queryset = Song.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('id',)
    paginate_by = 100
    paginate_by_param = 'page_size'
    max_paginate_by = 1000
    serializer_class = SongsSerializer