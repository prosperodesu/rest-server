from django.contrib import admin
from models import Song
from resources import SongResource
from import_export.admin import ImportExportModelAdmin

class SongAdmin(ImportExportModelAdmin):
    list_filter = ('type', 'singer')
    list_display = ('id', 'name', 'singer', 'type')
    resource_class = SongResource
    ordering = ('id', 'name', 'type', 'singer')

admin.site.register(Song, SongAdmin)