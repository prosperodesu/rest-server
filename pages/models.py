from django.db import models

class Song(models.Model):
    name = models.CharField(max_length=500)
    singer = models.CharField(max_length=500)
    type = models.CharField(default='Default', max_length=100)

    def __unicode__(self):
        return self.name