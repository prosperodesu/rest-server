from django.shortcuts import render
from pages.models import Song

def index(request):
    songs = Song.objects.raw('SELECT * FROM pages_song')[:100]
    return render(request, 'index.html', {'songs': songs})
